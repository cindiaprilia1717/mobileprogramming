//No.1 (Looping While)
console.log('LOOPING PERTAMA')
var flag = 1;
while (flag <= 10){
	console.log(flag*2 + ' - I love coding')
	flag++;
}

var flag = 10;
console.log('LOOPING KEDUA')
while (flag >= 1){
	console.log(flag*2 + ' - i will become a mobile developer')
	flag--;
}
console.log("")

//No.2 (Looping for)
for(var i = 1; i <= 20; i++) {
	if(i%2==0){
		console.log(i + '- Informatika');
	}else if(i%3==0){
		console.log(i + '- I Love Coding');
	}else{
		console.log(i + '- Teknik');
	}
}
console.log("")

//No.3 (Persegi panjang #)
var pp = '';
for(var i = 1; i <= 4; i++) {
	pp = '';
	for(var j = 1; j <= 8; j++) {
		pp += '#';
	}
	console.log(pp);
}
console.log("")

//No.4 (Tangga)
var pp1 = '';
for(var i = 1; i <= 7; i++) {
	pp1 = '';
	for(var j = 1; j <= i; j++) {
		pp1 += '#';
	}
	console.log(pp1);
}
console.log("")

//No.5 (Papan catur)
pp2 = '';
for(var i = 1; i <= 8; i++) {
	pp2 = '';
	for(var j = 1; j <= 8; j++) {
		if(i%2==0){
			if(j%2==0){
				pp2 += ' ';
			}else{
				pp2 += '#';
			}
		}else{
			if(j%2==0){
				pp2 += '#';
			}else{
				pp2 += ' ';
			}
		}
	}
	console.log(pp2);
}